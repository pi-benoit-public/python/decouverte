class Ville:

    nomVille = ""
    nomPays = ""
    nbHabitants = 0

    def __init__(self, nomVille, nomPays, nbHabitants):
        self.nomVille = nomVille
        self.nomPays = nomPays
        self.nbHabitants = nbHabitants

    def setNomVille(nomVille):
        self.nomVille = nomVille

    def getNomVille(self):
        return self.nomVille

    def setNomPays(nomPays):
        self.nomPays = nomPays

    def getNomPays(self):
        return self.nomPays

    def setNbHabitants(nombreHabitants):
        self.nbHabitants = nombreHabitants

    def getNbHabitants(self):
        return self.nbHabitants

    def display(self):
        print("Ville =", self.nomVille)
        print("Pays =", self.nomPays)
        print("Nombre d'habitants =", self.nbHabitants)
        return ""

"""grenoble = Ville("Grenoble","France", 168000)

print(grenoble.display())
print(grenoble.getNomPays())"""
